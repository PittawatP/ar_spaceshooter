﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#region Serializable classes
[System.Serializable]
public class EnemyWaves 
{
    //[Tooltip("time for wave generation from the moment the game started")]
    //public float timeToStart;

    [Tooltip("Enemy wave's prefab")]
    public GameObject wave;
}

#endregion

public class LevelController : MonoBehaviour {

    //Serializable classes implements
    public EnemyWaves[] enemyWaves; 
    private bool isSpawn = false;
    public float delay;
    private int start_delay;


    Camera mainCamera;   

    private void Start()
    {
        start_delay = (int)delay;
        mainCamera = Camera.main;
     
    }
    private void FixedUpdate()
    {
        SpawnWaveEndless();
        if (Time.time > GameManager.Instance.nextPhaseTime)
        {
            GameManager.Instance.nextPhaseTime += GameManager.Instance.period;
            delay -= 0.5f;
        }
        
    }
    public void SpawnWaveEndless()
    {
        if (!isSpawn)
            CountDelay(enemyWaves[(int)Random.Range(0, enemyWaves.Length)].wave);
        if (delay > 0)
        {
            isSpawn = false;
        }
    }
     private void CountDelay( GameObject Wave)
    {
     
        
        if (delay > 0)
        {
      
            delay-=Time.deltaTime;

            isSpawn = false;
        }
        else
        {
            Instantiate(Wave);
            delay = start_delay;
            isSpawn = true;
            
          
        }
    }

}
