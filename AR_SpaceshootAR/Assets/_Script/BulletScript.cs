﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    [SerializeField] private float bulletspeed = 10.0f;
    [SerializeField] private Vector3 BulletMovement;
    void Update()
    {
        Vector3 pos = transform.position;
        //this.transform.Translate(BulletMovement * bulletspeed * Time.deltaTime);
        pos += BulletMovement * bulletspeed * Time.deltaTime;

        transform.position = pos;
    }

    private void OnCollisionEnter(Collision other)
    {
        gameObject.SetActive(false);
        if (other.gameObject.CompareTag("Destroyer"))
        {
            gameObject.SetActive(false);
        }
    }

    public void SetDirection(Vector3 direction)
    {
        BulletMovement = direction.normalized;
    }
}
