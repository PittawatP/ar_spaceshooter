﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScriptPlayer : MonoBehaviour
{
    [SerializeField] private float bulletspeed = 10.0f;
    [SerializeField] private Vector3 BulletMovement = new Vector3(0,0,1);
    void Update()
    {
        this.transform.Translate(BulletMovement * bulletspeed * Time.deltaTime);

        if (this.transform.position.z > 35)
        {
            gameObject.SetActive(false);
        }
    }
    private void OnCollisionEnter(Collision other)
    {
        gameObject.SetActive(false);
        if (other.gameObject.CompareTag("Destroyer"))
        {
            gameObject.SetActive(false);
        }
        
    }
}
