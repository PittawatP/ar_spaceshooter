﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    // public static GameManager instance { get;private set; }

    public GameObject Enemy;
    public GameObject Enemy2;
    public GameObject Player;
    public GameObject HealthPack;


    public Vector3 spawnValue;

    public int EnemyCount;
    public int EnemyCount2;
    public int EnemyCurrentState { get; set; }

    public bool isStart = true;
    public bool isGameEnd = false;

    public float WaveTime = 5f;
    public float ItemSpawnTime = 10f;

    public LevelController Wave;

    public GameObject EndPanel;

    public GameObject ObjectLostPanel;
    public GameObject pausePanel;

    
    public TextMeshProUGUI ScoreTMPro;
    
    public float nextPhaseTime = 60.0f;
    public float period = 60.0f;
    public int Score { get; set; }
    
    public bool isPause { get; set; }
    // Start is called before the first frame update

    #region sigleton

    private static GameManager _instance;
    public static GameManager Instance => _instance;
    private void Awake()
    {
        if(_instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    #endregion

    void Start()
    {
        isPause = false;
    }


    // Update is called once per frame
    void Update()
    {
        
        if (isStart)
        {
            ObjectLostPanel.SetActive(false);
            Time.timeScale = 1;
            WaveTime -= Time.deltaTime;
            ItemSpawnTime -= Time.deltaTime;
            if (WaveTime <= 0)
            {
                Spawnwave();
                WaveTime = Random.Range(3,5);
                
                Debug.Log(EnemyCurrentState);
            }

            if (ItemSpawnTime <= 0)
            {
                SpawnItem();
                ItemSpawnTime = Random.Range(7, 10);
            }
        }
        else
        {
            ObjectLostPanel.SetActive(true);
            Time.timeScale = 0;
        }
        
        if (PlayerScript.isdead)
        {
            EndPanel.SetActive(true);
            Time.timeScale = 0;
        }
        else
        {
            EndPanel.SetActive(false);
        }
        
        

        if (Time.time > nextPhaseTime)
        {
            nextPhaseTime += period;
            EnemyScript.maxHealth += 0;
            // EnemyScript.maxShootTime -= 0.5f;
            // if (EnemyScript.maxShootTime <= 2)
            // {
            //     EnemyScript.maxShootTime = 3;
            // }
            EnemyAImpoint.maxShootTime -= 0.5f;
            if (EnemyAImpoint.maxShootTime <= 2)
            {
                EnemyAImpoint.maxShootTime = 2;
            }
        }

        if (isPause)
        {
            pausePanel.SetActive(true);
            Time.timeScale = 0;
        }

        ScoreTMPro.text = Score.ToString();
    }
    
    void Spawnwave()
    {
        // EnemyRandomNum = Random.Range(0, 100);
        // if (EnemyRandomNum > 50)
        // {
        //     EnemyType = 0;
        // }
        // else
        // {
        //     EnemyType = 1;
        // }
        EnemyCurrentState = Random.Range(1, 2);

        Quaternion spawnRotation = Quaternion.identity;
        //Instantiate(Enemy[EnemyType], spawnPos, spawnRotation);
        for (int i = 0; i < EnemyCount; i++)
        {
            Vector3 spawnPos = new Vector3(Random.Range((int) -spawnValue.x, (int) spawnValue.x), spawnValue.y,
                spawnValue.z);
            Enemy = ObjectPooling.SharedInstance.GetPooledObject("Enemy");
            if (Enemy != null)
            {
                Enemy.transform.position = spawnPos;
                Enemy.transform.rotation = spawnRotation;
                Enemy.SetActive(true);
            }
        }

        for (int i = 0; i < EnemyCount2; i++)
        {
            Vector3 spawnPos2 = new Vector3(Random.Range((int) -spawnValue.x, (int) spawnValue.x), spawnValue.y,
                spawnValue.z);
            Enemy2 = ObjectPooling.SharedInstance.GetPooledObject("Enemy2");
            if (Enemy2 != null)
            {
                Enemy2.transform.position = spawnPos2;
                Enemy2.transform.rotation = spawnRotation;
                Enemy2.SetActive(true);
                
            }
        }
    }

    void SpawnItem()
    {
        Quaternion spawnRotation = Quaternion.Euler(90,0,0);
        Vector3 ItemspawnPos = new Vector3(Random.Range((int) -spawnValue.x - 5, (int) spawnValue.x - 5), spawnValue.y,
            spawnValue.z);
        HealthPack = ObjectPooling.SharedInstance.GetPooledObject("HealthPack");
        if (HealthPack != null)
        {
            HealthPack.transform.position = ItemspawnPos;
            HealthPack.transform.rotation = spawnRotation;
            HealthPack.SetActive(true);
        }

    }

    public void IsFoundObject()
    {
        Player.SetActive(true);
        isStart = true;
    }
    
    public void IsLostObject()
    {
        Player.SetActive(false);
        isStart = false;
    }

    public void RestartButton()
    {
        PlayerScript.isdead = false;
        PlayerScript.Health = 100;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void PauseButton()
    {
        isPause = true;
    }

    public void ResumeButton()
    {
        pausePanel.SetActive(false);
        isPause = false;
    }
}