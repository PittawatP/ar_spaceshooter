﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour
{
    public float Rotatespeed = 2;
    // Start is called before the first frame update
    void Start()
    {
        Rotatespeed = Random.Range(-5, 5);
        if (Rotatespeed == 0)
        {
            Rotatespeed++;
        }
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Rotate(new Vector3(0,1*Rotatespeed,0));
    }
}
