﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSpawner : MonoBehaviour
{
    Vector3 MoveX = new Vector3(10f, 0, 0);
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Translate(MoveX * Time.deltaTime);
        if (this.transform.position.x >= 22)
        {
            this.transform.position = new Vector3(22f, 0.5f, 17f);
            MoveX.x *= -1f;
            
        }
        
        if (this.transform.position.x <= -22)
        {
            this.transform.position = new Vector3(-22f, 0.5f, 17f);
            MoveX.x *= -1f;
            
        }
    }
}
