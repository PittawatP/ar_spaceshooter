﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemyScript : MonoBehaviour
{
    
    

    
    public int Health = 100;
    public static int maxHealth = 100;
    public float speed = -2;
    public Vector3 Movement = new Vector3(0, 0, 1);
    [SerializeField] private bool IsShip = false;
    //[SerializeField] private float TimeDelay;
    [SerializeField] private float bulletSpeed = 10.0f;
    [SerializeField] private GameObject bullet;
    [SerializeField] private GameObject AimPoint;
    
    public ParticleSystem OnDeadPS;
    public GameObject[] itemDrop;
    public int score = 10;
    [SerializeField] private int randomNum;
    [SerializeField] private int RNG;
    [SerializeField] private bool isDrop;
    // public static float maxShootTime = 10;
    // public static float minShootTime = 1;

    public float amplitude;
    public float period;
    public float shift;
    public float yChange;
    private float newX;
    private float newY;
    private Rigidbody mRigidbody;
    private Vector3 direction;
    [Tooltip("VFX prefab generating after destruction")]
    public GameObject destructionVFX;
    public GameObject hitEffect;
    public int shotChance; //probability of 'Enemy's' shooting during tha path
    public float shotTimeMin, shotTimeMax; //max and min time for shooting from the beginning of the path
    // Start is called before the first frame update


    void Start()
    {
        mRigidbody = GetComponent<Rigidbody>();
        Invoke("ActivateShooting", Random.Range(shotTimeMin, shotTimeMax));
    }
    void ActivateShooting()
    {
        if (Random.value < (float)shotChance / 100)                             //if random value less than shot probability, making a shot
        {
            EnemyAImpoint EAimpoint = GetComponentInChildren<EnemyAImpoint>();
            EAimpoint.Fire();

        }
    }
    // Update is called once per frame
    public void Update()
    {
        
        if (Health <= 0)
        {
            gameObject.SetActive(false);
            Dead();
            Health = maxHealth;
        }
        
        
        if (IsShip)
        {
            // TimeDelay -= Time.deltaTime;
            // if (TimeDelay <= 0)
            // {
            //     //Shooting();
            //     TimeDelay = Random.Range(minShootTime, maxShootTime);
            // }
        }

        //Movement State
        this.transform.Translate(Movement * speed * Time.deltaTime); //basic movement


        if (this.transform.position.z < -40f)
        {
            gameObject.SetActive(false);
        }
        
        

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (IsShip && collision.gameObject.CompareTag("Bullet"))
        {
            GetDamage(40);
            AudioManager.instance.Play("Enemy hit");
        }
        if (!IsShip && collision.gameObject.CompareTag("Bullet"))
        {
            GetDamage(100);
            
        }

        if (collision.gameObject.CompareTag("Destroyer") || collision.gameObject.CompareTag("Player"))
        {
            gameObject.SetActive(false);
        }
    }



    void Dead()
    {
       // OndeadEffect();
        if (!IsShip)
        {
            GameManager.Instance.Score += score;
        }


        if (IsShip)
        {
            GameManager.Instance.Score += (score + 10);
        }


        RNG = Random.Range(0, 100);
        Quaternion spawnrotation = Quaternion.Euler(90,0,0);
        if (RNG > 85)
        {
            isDrop = true;
            if (isDrop)
            {
                if (RNG > 90 && RNG <= 95)
                {
                    randomNum = 0;
                    Instantiate(itemDrop[randomNum], this.transform.position, spawnrotation);
                    isDrop = false;
                }

                if (RNG > 95 && RNG <= 100)
                {
                    randomNum = 1;
                    Instantiate(itemDrop[randomNum], this.transform.position, spawnrotation);
                    isDrop = false;
                }

                
            }
        }
    }
    void Destruction()
    {
     GameObject eff=   Instantiate(destructionVFX, transform.position, Quaternion.identity);
        Destroy(eff);
    }
    public void GetDamage(int damage)
    {
        Health -= damage;           //reducing health for damage value, if health is less than 0, starting destruction procedure
        if (Health <= 0)
        {
            Destruction();
            AudioManager.instance.Play("Enemy Dead");
        }
        else
            Instantiate(hitEffect, transform.position, Quaternion.identity, transform);
    }

    //public void OndeadEffect()
    //{
    //    Vector3 EnemyPos = gameObject.transform.position;
    //    Vector3 spawnPos = new Vector3(EnemyPos.x, EnemyPos.y, EnemyPos.z);
    //    GameObject effect = Instantiate(OnDeadPS.gameObject, spawnPos, Quaternion.identity);

    //    ParticleSystem.MainModule mm = effect.GetComponent<ParticleSystem>().main;
    //    Destroy(effect, OnDeadPS.main.startLifetime.constant);
    //}
}