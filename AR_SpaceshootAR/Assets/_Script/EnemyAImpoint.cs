﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAImpoint : MonoBehaviour
{
    public GameObject EnemyBullet;
    [SerializeField] private float TimeDelay = 2f;
    private Vector3 direction;
    public static float maxShootTime = 5;
    public static float minShootTime = 1;
    // Start is called before the first frame update
 
    // Update is called once per frame
    void Update()
    {
        // TimeDelay -= Time.deltaTime;
        // if (TimeDelay <= 0)
        // {
        //     Fire();
        //     
        //     TimeDelay = Random.Range(minShootTime, maxShootTime);
        // }
    }

    public void Fire()
    {
        AudioManager.instance.Play("Enemy Shoot");
        GameObject player = GameObject.FindWithTag("Player");
        GameObject go = ObjectPooling.SharedInstance.GetPooledObject("EnemyBullet");
        if (go != null)
        {
            go.transform.position = transform.position;
            go.transform.rotation = transform.rotation;
            direction = player.transform.position - go.transform.position;
            go.GetComponent<BulletScript>().SetDirection(direction);
            go.SetActive(true);
        }
    }

   
}
