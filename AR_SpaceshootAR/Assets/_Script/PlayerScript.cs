﻿using System;
using UnityEngine;
using UnityEngine.UI;


public class PlayerScript : MonoBehaviour
{
    public GameObject bullet;
    public Transform aimPoint;
    public Transform[] aimpointArray;
    public float bulletSpeed = 10.0f;
    public static int Health = 100;

    public float TimeDelay = 2.0f;
    public float ShootTime = 2;
    public float ShootTimeReset;
    public float BuffTime = 10.0f;
    public float FastShotTime = 5;
    public float fastShootTimeReset;

    public HealthBar healthbar;

    public GameObject ImageTrack;

    public static bool isdead = false;

    public bool isDouble;
    public bool isFast;

    public int bulletCount = 2;

    public Image isDoubleImage;

    public Image isFastImage;


    // Start is called before the first frame update

    // Update is called once per frame


    void Update()
    {
        if (!GameManager.Instance.isPause)
        {
            TimeDelay -= Time.deltaTime;
            if (TimeDelay <= 0)
            {
                Shoot();
                if (!isFast)
                {
                    ShootTime = ShootTimeReset;
                    TimeDelay = ShootTime;
                    AudioManager.instance.Play("Player Shoot");
                }


                if (isFast)
                {
                    TimeDelay = fastShootTimeReset;
                    AudioManager.instance.Play("Player Shoot");
                }
            }

            if (Health <= 0)
            {
                isdead = true;
            }

            if (isDouble)
            {
                isDoubleImage.color = Color.white;
                BuffTime -= Time.deltaTime;
                if (BuffTime <= 0)
                {
                    isDouble = false;
                }
            }
            else
            {
                isDoubleImage.color = Color.gray;
            }

            if (Health > 100)
            {
                Health = 100;
            }

            if (isFast)
            {
                isFastImage.color = Color.white;
                FastShotTime -= Time.deltaTime;
                if (FastShotTime <= 0)
                {
                    isFast = false;
                }
            }
            else
            {
                isFastImage.color = Color.gray;
            }
        }


        this.transform.position = new Vector3(300 * ImageTrack.transform.position.x, 0.5f, -20);
        healthbar.SetHealth(Health);
    }

    void Shoot()
    {
        if (!isDouble)
        {
            GameObject go = ObjectPooling.SharedInstance.GetPooledObject("Bullet");
            if (go != null)
            {
                go.transform.position = aimPoint.transform.position;
                go.transform.rotation = aimPoint.transform.rotation;
                go.SetActive(true);
                
            }

            // Rigidbody Rb = go.GetComponent<Rigidbody>();
            // Rb.AddForce(new Vector3(0, 0, bulletSpeed), ForceMode.Impulse);
        }

        if (isDouble)
        {
            for (int i = 0; i < bulletCount; i++)
            {
                GameObject go = ObjectPooling.SharedInstance.GetPooledObject("Bullet");
                if (go != null)
                {
                    go.transform.position = aimpointArray[i].transform.position;
                    go.transform.rotation = aimpointArray[i].transform.rotation;
                    go.SetActive(true);
                }

                // Rigidbody Rb = go.GetComponent<Rigidbody>();
                // Rb.AddForce(new Vector3(0, 0, bulletSpeed), ForceMode.Impulse);
            }
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Enemy") || other.gameObject.CompareTag("EnemyBullet") || other.gameObject.CompareTag("Enemy2"))
        {
            Health -= 20;
            AudioManager.instance.Play("Player hit");
        }

        if (Health < 100 && other.gameObject.CompareTag("HealthPack"))
        {
            Health += 50;
            AudioManager.instance.Play("ItemPickUp");
        }

        if (other.gameObject.CompareTag("DoubleBullet"))
        {
            isDouble = true;
            BuffTime = 10.0f;
            AudioManager.instance.Play("ItemPickUp");
        }

        if (other.gameObject.CompareTag("FastShot"))
        {
            isFast = true;
            FastShotTime = 5;
            AudioManager.instance.Play("ItemPickUp");
        }
    }
}